""" Test of the csv -> object """

import csv
from random import randint, shuffle
from tissues import Muscle

filename = "leg.csv"

mus_arr = []
mus_dic = {}

with open(filename) as leg:
    read = csv.reader(leg)
    for row in read:
        if row[0] != "Name":
            mus_dic[row[0]] = Muscle(row[0],row[1],row[2],row[3])
            mus_arr.append(row[0])

def quiz(array, mode=0):
    shuffle(array)
    if mode == 0:
        for muscle in array:
            para = randint(0,2)
            if para == 0:
                print "Q: What is the origin of {0}?".format(muscle)
                raw_input("> ")
                print "A: {0}.\n".format(mus_dic[muscle].ori)
            elif para == 1:
                print "Q: What is the insertion of {0}".format(muscle)
                raw_input("> ")
                print "A:i {0}.\n".format(mus_dic[muscle].ins)
            elif para == 2:
                print "Q: What is the innervation of {0}".format(muscle)
                raw_input("> ")
                print "A: {0}.\n".format(mus_dic[muscle].neu)
    elif mode == 1:
        for muscle in array:
            print "Q: What is the origin of {0}?".format(muscle)
            raw_input("> ")
            print "A: {0}.\n".format(mus_dic[muscle].ori)
    elif mode == 2:
        for muscle in array:
            print "Q: What is the insertion of {0}?".format(muscle)
            raw_input("> ")
            print "A: {0}.\n".format(mus_dic[muscle].ins)
    elif mode == 3:
        for muscle in array:
            print "Q: What is the innervation of {0}?".format(muscle)
            raw_input("> ")
            print "A: {0}.\n".format(mus_dic[muscle].neu)

for i in range(1,4):
    quiz(mus_arr, i)
